import quandl
import pandas as pd
import numpy as np
import datetime

from sklearn.linear_model import LinearRegression
from sklearn import preprocessing, cross_validation, svm


df = quandl.get("WIKI/AMZN")

df = df[['Adj. Close']]

forecast_out = 30
df['Prediction'] = df[['Adj. Close']].shift(-forecast_out)

X = np.array(df.drop(['Prediction'], 1))
X = preprocessing.scale(X)

X_forecast = X[-forecast_out:]
X = X[:-forecast_out]

y = np.array(df['Prediction'])
y = y[:-forecast_out]

X_train, X_test, Y_train, Y_test = cross_validation.train_test_split(X, y, test_size = 0.2)

clf = LinearRegression()
clf.fit(X_train, Y_train)

confidence = clf.score(X_test, Y_test)

print("Confidence : ", confidence)

forecast_prediction = clf.predict(X_forecast)
print(forecast_prediction)